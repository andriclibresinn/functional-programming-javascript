const valLabelExists = (label, value) => (!label && value)
const valLabelSame = (label, value) => (label === value)
const valisLabel = (label, value) => (valLabelExists(label, value) && valLabelSame(label, value))

getVehicleModelList = (modelList, defaultSelection) => {
  let vehicleModelLabel = ''
  let label = ''
  let value = ''
  
  const nextSelectOptions = modelList.map(item => {
    label = item.code
    value = item.description

    if (valisLabel(label, value)) {
      vehicleModelLabel = value
    } else if (value === 'OTHER') {
      vehicleModelLabel = label
    } else if (label && value) {
      const startsWithModel = new RegExp('^' + value)
      const startsWithModelAndSpace = new RegExp('^' + value + ' ')
      const startsWithModelSlashModel = new RegExp('^' + value + '\/' + value)

      if (startsWithModelAndSpace.test(label) || startsWithModelSlashModel.test(label)) {
        vehicleModelLabel = value
      } else if (startsWithModel.test(label) && !startsWithModelAndSpace.test(label)) {
        vehicleModelLabel = value + (label.substr(value.length).split(' ')[0])
      } else {
        vehicleModelLabel = label
      }
    } else {
      vehicleModelLabel = label
    }

    const rObj = {
      data: item.code,
      text: vehicleModelLabel,
      value: item.value,
    }

    return rObj
  })
  const vehicleModelList = uniqBy(nextSelectOptions, (model) => (model.text))
  vehicleModelList.unshift(defaultSelection)
  return vehicleModelList
}
