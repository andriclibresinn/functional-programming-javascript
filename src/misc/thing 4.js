'use strict'
// const log = {
//   encAgreementId: ${encAgreementId}
//   encCorrelationId: ${encCorrelationId}
//   dbAgreementId: ${dbAgreementId}
//   dbAdminTrnId: ${dbAdminTrnId}
// }

const fn = (...args) => {
  // function fn(encAgreementId, encCorrelationId, dbAgreementId, dbAdminTrnId) {
  const hey = args.map((item) => (`${item}: ${item}`))

  console.log(hey.join(', '))
  // console.log(encAgreementId)
  // console.log(JSON.stringify(arguments, null, 2))

  // logger.info(`encAgreementId: ${encAgreementId}, encCorrelationId: ${encCorrelationId}, dbAgreementId: ${dbAgreementId},dbAdminTrnId: ${dbAdminTrnId},`);
  return 'Data persisted'
}

fn('one', 'two', 'three', 'four')