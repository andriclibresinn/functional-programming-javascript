const Maybe = require('monet').Maybe

// console.log("hello world".some())
console.log(Maybe.Some('val'))

const next = Maybe.Some(123).map((val) => (
  val + 1
))


console.log(next.just())

console.log(Maybe.Some('thingy').orJust('val'))