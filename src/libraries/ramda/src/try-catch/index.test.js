/* eslint-disable no-console */
import chai from 'chai'
import { tryCatchExamples } from './'

const { expect } = chai

describe('./try-catch/index.js', () => {
  describe.skip('tryCatchExamples.tryCatchExample1', () => {
    it('Should work', async () => {
      expect(tryCatchExamples.tryCatchExample1('asd')).to.equal('pizzaaaed')
      expect(tryCatchExamples.tryCatchExample1('asd123')).to.equal('failed')
    })
  })

  describe('tryCatchExamples.tryCatchExample2', () => {
    it('Should work', async () => {
      expect(tryCatchExamples.tryCatchExample2('asd')).to.equal('passed yo!')
      // expect(tryCatchExamples.tryCatchExample2('asd123')).to.equal('failed')
    })
  })

  describe.skip('do an async version', () => {
    it('Should work', async () => {
      expect(true).to.equal(true)
    })
  })

  describe.skip('default some of this data', () => {
    it('Should work', async () => {
      // https://ramdajs.com/docs/#defaultTo
      expect(true).to.equal(true)
    })
  })
})
