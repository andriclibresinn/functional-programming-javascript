/* eslint-disable no-console */
import { tryCatch, ifElse } from 'ramda'

// https://ramdajs.com/docs/#ifElse (condition, onTrue, onFalse)
export const get1 = (thing) => {
  if (thing === 'asd') {
    return 'pizzaaaed'
  }

  throw new Error('butts')
}

// Functional version of the above.
export const isASD = (param) => (param === 'asd')
export const asdPassed = () => ('passed yo!')
export const asdFailed = () => ('This is a failure :(')
export const get2 = ifElse(isASD, asdPassed, asdFailed)

export const logger = (exception, param1) => {
  console.log('LOGGER - logged this param:', param1)
  return 'failed'
}

export const tryCatchExamples = {
  tryCatchExample1: tryCatch(get1, logger),
  tryCatchExample2: tryCatch(get2, logger),
}
