/* eslint-disable import/no-unresolved, import/extensions */
import R from 'ramda'
import services from './some/path/to/services'
import { error } from './some/path/to/logger'

/**
 * What the old code looked like. What can we do to simplify, make it
 * bulletproof and make it more testable?
 *
 * This is, of course, going to ignore, until a future date, Composition,
 * Currying & Moands (Maybe type).
 *
 * export const fetchESignAvailable = async (policyNumber) => {
 *   let eSignAvailable = false
 *   try {
 *     const response = await services.post(EDOCS_AVAILABLE, { policyNumber: [policyNumber] })
 *     eSignAvailable = hasCeremony(response)
 *       ? eSign(response.data.ceremonies[0])
 *       : false
 *   } catch (error) {
 *     console.log('esign error')
 *     logger.error('Error', logger.CODES.API_CALL_FAILED, 'Get E-Sign Ceremonies Error', error)
 *   }
 *   return eSignAvailable
 * }
 */

export const defultShape = () => ({
  sessionType: '',
  ceremonyStatus: '',
})

export const shapedDocuments = (obj) => ({
  sessionType: R.propOr('', 'ceremonies[0].sessionType')(obj),
  ceremonyStatus: R.propOr('', 'ceremonies[0].ceremonyStatus')(obj),
})

export const isESignable = (obj) => (R.allPass([
  R.propEq(obj, 'ceremonies[0].sessionType'),
  !R.propEq(obj, 'ceremonies[0].ceremonyStatus'),
]))

export const tryer = () => ((R.compose(shapedDocuments, services.documents)))
export const catcher = () => ((R.compose(error, defultShape)))

export const eSignAvailable = async (policyNumber) => (
  isESignable(await R.tryCatch(tryer, catcher)(policyNumber))
)
