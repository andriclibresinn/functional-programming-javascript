/* eslint-disable no-console */
import R, { cond, always } from 'ramda'

export const parseResponseForNextSelect = (listType, selectName, response) => {
  let label
  switch (selectName) {
    case 'year':
      label = 'Make'
      break
    case 'make':
      label = 'Model'
      break
    case 'model':
      label = 'Trim'
      break
    case 'trim':
      label = 'Body'
      break
    default:
      break
  }
  const defaultSelection = {
    text: label,
    value: label,
  }

  if (listType === 'modelList') {
    return this.getVehicleModelList(response, defaultSelection)
  } else if (listType === 'trimList') {
    return this.getVehicleTrimList(response, defaultSelection)
  }

  const nextSelectOptions = response.map((item) => {
    const rObj = {
      data: item.code,
      text: item.description,
      value: item.value,
    }
    return rObj
  })

  nextSelectOptions.unshift(defaultSelection)
  return nextSelectOptions
}

// ------------------------------------------------------------------------
export const labelMap = () => ({
  year: 'Make',
  make: 'Model',
  model: 'Trim',
  trim: 'Body',
})

export const dataTemplate = (item) => ({
  data: R.propOr(item, 'code', ''),
  text: R.propOr(item, 'description', ''),
  value: R.propOr(item, 'value', ''),
})

export const isTypeModel = (listType) => (listType === 'modelList')
export const isTypeTrim = (listType) => (listType === 'trimList')
export const label = (select) => (R.prop(select, labelMap()))
export const getVehicleMakeList = () => { }
export const getVehicleTrimList = () => { }

// TODO: Memoize me.
export const defaultSelection = (selectName) => ({
  text: label(selectName),
  value: label(selectName),
})

export const listType = cond([
  [isTypeModel, always(getVehicleMakeList())],
  [isTypeTrim, always(getVehicleTrimList())],
])

export const nextSelect = (listTypeA, selectName, response) => {
  listType(listTypeA, response, defaultSelection())

  const nextSelectOptions = response.map(dataTemplate)

  nextSelectOptions.unshift(defaultSelection())
  return nextSelectOptions
}
