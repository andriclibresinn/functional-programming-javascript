/* eslint-disable */
/* eslint-disable no-console */
import R from 'ramda'

export const defultShape = () => ({
  type: 'asd',
  status: 'asd',
})

var favorite = R.prop('favoriteLibrary')
var favoriteWithDefault = R.propOr('none', 'type')

// console.log('fav:', favorite({}))
// console.log('fav:', favorite({ favoriteLibrary: '123' }))
console.log('fav w/default:', favoriteWithDefault(defultShape()))
console.log('fav w/default:', favoriteWithDefault({}))
console.log('straight call:', R.propOr('none', 'type')(defultShape()))

// // The default shape.
// export const defultShape = () => ({
//   type: '',
//   status: '',
// })

// // Do the "api" call.
// export const documents = (thing) => {
//   console.log('thing', thing)

//   const docs = {
//     type: 'andric',
//     status: 'andric also',

//     // type: thing || 'andric',
//     // status: thing || 'andric also',

//     // typeWrong: '',
//     // statusWrong: '',
//   }

//   console.log('documents: ', JSON.stringify(docs, null, 2))
//   console.log('------------------------------------------------------------')

//   return docs
// }

// const butts = R.propOr('type', '')

// console.log(butts(defultShape()))

// // Convert
// export const transformer = (obj) => {
//   console.log('obj', obj)
//   console.log('R.propOr(obj)', R.propOr('type', 'empty')(obj))
//   console.log('R.propOr(obj)', R.propOr('status', 'empty')(obj))
//   console.log()
//   const hey = {
//     type: R.propOr(obj, 'type', 'empty'),
//     status: R.propOr(obj, 'status', 'empty'),
//   }

//   // console.log('hey', hey)
//   console.log('transformer: ', JSON.stringify(hey, null, 2))
//   console.log('------------------------------------------------------------')

//   return hey
// }

// export const tryer = R.compose(transformer, documents)
// // export const tryer = R.compose(documents, transformer)
// // export const tryer = R.pipe(transformer, documents)

// // export const catcher = () => ((R.compose(error, defultShape)))

// // export const eSignAvailable = async (policyNumber) => (
// //   isESignable(await R.tryCatch(tryer, catcher)(policyNumber))
// // )

// // const didThing = tryer('someArgument')
// const didThing = tryer()

// console.log('tryer:', JSON.stringify(didThing, null, 2))
// console.log('------------------------------------------------------------')
