/* eslint-disable no-console */
// import R from 'ramda'
import chai from 'chai'

import { ceremony } from './mocks'
import { ramdaExample, signer, isNameAndric, dispatch } from './'

const { expect } = chai

describe('./conditionals/index.js', () => {
  describe('if check', () => {
    it('Ramda example', async () => {
      expect(ramdaExample(0)).to.equal('water freezes at 0°C')
      expect(ramdaExample(50)).to.equal('nothing special happens at 50°C')
      expect(ramdaExample(100)).to.equal('water boils at 100°C')
    })

    it('determines if this text is "andric"', async () => {
      expect(isNameAndric(0)).to.equal('Nah son')
      expect(isNameAndric({ andric: 'nah' })).to.equal('Nah son')
      expect(isNameAndric(false)).to.equal('Nah son')
      expect(isNameAndric(null)).to.equal('Nah son')
      expect(isNameAndric('andy')).to.equal('Nah son')
      expect(isNameAndric('andric 1')).to.equal('Nah son')

      expect(isNameAndric('ANdric')).to.equal('yip')
      expect(isNameAndric('ANDRIC')).to.equal('yip')
      expect(isNameAndric('andric')).to.equal('yip')
    })

    it('runs a dispatch table w/o passing params', async () => {
      expect(dispatch(true)).to.contain('  ')
      expect(dispatch(false)).to.contain('different')
    })
  })

  describe('Both predicicates must pass', () => {
    it('Let us try this out', async () => {
      expect(signer(ceremony.good)).to.equal(true)
      expect(signer(ceremony.bad)).to.equal(false)
    })
  })
})
