/* eslint-disable no-console, arrow-body-style */
import R, { always, cond, equals } from 'ramda'

/**
 * Ramda example
 */
export const ramdaExample = cond([
  [equals(0), always('water freezes at 0°C')],
  [equals(100), always('water boils at 100°C')],
  [R.T, (temp) => `nothing special happens at ${temp}°C`],
])

// TODO: Test this example.
export const ramdaIfElseExample = R.ifElse(
  // #1 The predicate.
  R.has('count'),

  // #2 Passed conditional; run this function.
  (param) => { console.log('passed param:', param); return 'passed yo!' },

  // #3: Failed conditional; run this function. You would typcially
  // still want to return something
  () => { throw new Error('THIS FAILED OMG!') }
)

console.log(ramdaIfElseExample())
console.log(ramdaIfElseExample({ count: 'something' }))
console.log(ramdaIfElseExample(''))

/**
 * Convert old signer function from single to multiple.
 *
 * Old Reference:
 *    export const signer = (ceremonyData) => (
 *      ceremonyData.sessionType === 'signer' && ceremonyData.status !== 'complete'
 *    )
 *
 * Issues where aren't big but this expanded does become a problem. We have 3 pieces of logic.
 * Two truthy comparisons and one &&.
 */
export const type = (ceremony) => (ceremony.sessionType === 'signer')
export const status = (ceremony) => (ceremony.status !== 'complete')
export const signer = (R.both(type, status))

/**
 * Is name andric
 */
export const isAndric = (name) => (String(name).toLowerCase() === 'andric')
export const isNameAndric = cond([
  [isAndric, always('yip')],

  // Used as the default case otherwise we gotta curry this with
  // one more conditional. Or use a Maybe.
  [R.T, always('Nah son')],
])

/**
 * Dispatcher methods.
 */

export const apiText = (thing = '') => (always(`do a ${thing} thing!`)())
export const apiCallTrue = () => {
  // console.log(apiText())
  return (apiText())
}

export const apiCallFalse = () => {
  // console.log(apiText('different'))
  return (apiText('different'))
}

export const dispatch = (cond([
  [equals(true), apiCallTrue],
  [equals(false), apiCallFalse],
]))
