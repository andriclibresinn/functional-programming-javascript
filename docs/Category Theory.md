# Category Theory - Bartosz Milewski
## Category Theory 1.1: Motivation and Philosophy
* Composibility: Break things down into smaller easier to understand ... things
* Abstract "Drawn away" or to hide the details
  - Hiding things.
* Data race: Sharing data (pointers, referances) & mutating data.
  - Really bad for threads.
* If we don't find structure we give up. It's difficult for our brains. Category Theory is less about the math and physics of stuff and more about the categorizing of stuff.

## [Category Theory 1.2: What is a category?](https://www.youtube.com/watch?v=p54Hd7AmVFU)
(18.27)
1. Is a "bunch" of objects.

* Abstraction
  - Things that were different because of "details" become identical.
  - ie: Coins
    - Their differences
      - Different under microscope (scratches, dents et al)
      - Perhaps different atom configurations
      - Different serial number
      - Different insepctor
      - Different color
    - Their similarties
      - Pull back and we can use them interchangeably

* Composition
* Identity
* 
* 

More videos on Category Theory
* [Category Theory Lulz - Ken Scambler](https://www.youtube.com/watch?v=jDhMDgU7Koc)
* [Category Theory in Life - Eugenia Cheng](https://www.youtube.com/watch?v=ho7oagHeqNc)
* [Category Theory, The essence of interface-based design - Erik Meijer](https://www.youtube.com/watch?v=JMP6gI5mLHc)
* [Category Theory for the Working Hacker by Philip Wadler](https://www.youtube.com/watch?v=V10hzjgoklA)
* []()
* []()
* []()
* []()

### Glossary:
`Category`

`Objects`
  - No properties
  - Think of an object as something to identify the ends of arrows.

`Morphisms`
  - No more properties other than it points from one object to another. Has direction?

```
Morphism example:

a ----------------> b
          ƒ
```